#include "miditrigger.h"
#include "config.h"

MidiTrigger::MidiTrigger(SettingsData& settings)
{
    seqNotifier = NULL;
    seq_handle_allocated = false;
    in_port = -1;
    if (!settings.getEnableMidiTrigger()) {
        return;
    }
    midiChannel = settings.getMidiChannel()-1;
    recordNote = settings.getMidiNote();
    pauseNote = recordNote+1;
    stopNote = recordNote+2;

    if (open_seq() == 0) {
        initSeqNotifier();
    }
}

MidiTrigger::~MidiTrigger()
{
    if (seqNotifier != NULL) {
        delete seqNotifier;
        seqNotifier = NULL;
    }

    if (in_port >= 0) {
        snd_seq_delete_simple_port(seq_handle, in_port);
        in_port = -1;
    }

    if (seq_handle_allocated) {
        snd_seq_close(seq_handle);
        seq_handle_allocated = false;
    }
}

int MidiTrigger::open_seq()
{
    int err;

    err = snd_seq_open(&seq_handle, "default", SND_SEQ_OPEN_DUPLEX, 0);
    if (err < 0) {
        qWarning("Error opening ALSA sequencer (%s).", snd_strerror(err));
        return(-1);
    }
    seq_handle_allocated = true;

    err = snd_seq_set_client_name(seq_handle, PACKAGE);
    if (err < 0) {
        qWarning("Error setting ALSA client name (%s).", snd_strerror(err));
        return(-1);
    }

    in_port = snd_seq_create_simple_port(seq_handle, "in_1",
        SND_SEQ_PORT_CAP_WRITE | SND_SEQ_PORT_CAP_SUBS_WRITE,
        SND_SEQ_PORT_TYPE_APPLICATION);
    if (in_port < 0) {
       qWarning("Error creating sequencer port (%s).", 
           snd_strerror(in_port));
       return(-1);
    }

    return(0);
}


int MidiTrigger::initSeqNotifier() {

    int alsaEventFd = 0;

    struct pollfd pfd[1];
    snd_seq_poll_descriptors(seq_handle, pfd, 1, POLLIN);
    alsaEventFd = pfd[0].fd;
    seqNotifier = new QSocketNotifier(alsaEventFd,
            QSocketNotifier::Read);
    QObject::connect(seqNotifier, SIGNAL(activated(int)),
            this, SLOT(midiAction()));
    return(0);
}

void MidiTrigger::midiAction() {

    snd_seq_event_t *ev;

    do {
        snd_seq_event_input(seq_handle, &ev);
        if ((ev->type == SND_SEQ_EVENT_NOTEON) && 
            (ev->data.control.channel == midiChannel))
        {
            int n = ev->data.note.note;
            if (n == recordNote) {
                emit record();
            }
            if (n == pauseNote) {
                emit pause();
            }
            if (n == stopNote) {
                emit stop();
            }
        }
        snd_seq_free_event(ev);
    } while (snd_seq_event_input_pending(seq_handle, 0) > 0);
}

