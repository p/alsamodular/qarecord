#include <QMessageBox>
#include <QStringList>
#include "settingsdialog.h"
#include "config.h"
#include <alsa/asoundlib.h>

#if SND_LIB_VERSION >= ((1 << 16) + (0 << 8) + 14)


void getAlsaDeviceItems(QStringList& list)
{
    void **allhints, **p;
    if (snd_device_name_hint(-1, "pcm", &allhints) < 0) {
        return;
    }
    p = allhints;
    for(p = allhints; *p != NULL; p++) {
        char* ioid = snd_device_name_get_hint(*p, "IOID");
        bool canCapture = (ioid == NULL) || (strcmp(ioid, "Input") == 0);
        if (ioid != NULL) {
            free(ioid);
        }
        if (!canCapture) {
            continue;
        }
        char* name = snd_device_name_get_hint(*p, "NAME");
        if (name != NULL) {
            list += name;
            free(name);
        }
    }
    snd_device_name_free_hint(allhints);

    /* Hack to get rid of all unnecessary "surround" entries */
    for (int i = 0; i < list.size(); i++) {
        int j = list.at(i).indexOf(":CARD=");
        if (j >= 0) {
            list[i] = list[i].replace(0, j, "plughw");
        }
    }
    list.sort();
    for (int i = list.size()-2; i >= 0; i--) {
        if (list.at(i) == list.at(i+1)) {
           list.removeAt(i+1);
        }
    }
}
#else
void getAlsaDeviceItems(QStringList& list)
{
}
#endif

SettingsDialog::SettingsDialog(QWidget *parent) : QDialog(parent)
{
    ui.setupUi(this);

    QStringList alsaDeviceItems;
    getAlsaDeviceItems(alsaDeviceItems);
    ui.alsaDeviceComboBox->addItems(alsaDeviceItems);

    connect(ui.driverComboBox, SIGNAL(currentIndexChanged(int)),
            this, SLOT(updateEnabled()));
    connect(ui.midiTriggerEnabledCheckBox, SIGNAL(stateChanged(int)),
            this, SLOT(updateEnabled()));

    connect(ui.resetButton, SIGNAL(clicked()), this, SLOT(resetToDefault()));
    connect(ui.loadButton, SIGNAL(clicked()), this, SLOT(loadFromFile()));
    connect(ui.saveButton, SIGNAL(clicked()), this, SLOT(saveToFile()));

}

void SettingsDialog::loadFromSettings(SettingsData& settings)
{
    ui.channelsSpin->setValue(settings.getChannels());
    ui.bitComboBox->setCurrentIndex(settings.getEnable32bit() ? 1 : 0);
    ui.splitSpin->setValue(settings.getSplitMB());

    ui.midiTriggerEnabledCheckBox->setChecked(settings.getEnableMidiTrigger());
    ui.midiChannelSpin->setValue(settings.getMidiChannel());
    ui.midiNoteSpin->setValue(settings.getMidiNote());

    ui.driverComboBox->setCurrentIndex(settings.getEnableJack() ? 1 : 0);
    ui.alsaDeviceComboBox->setEditText(settings.getAlsaPcmName());
    ui.rateSpin->setValue(settings.getRate());
    ui.periodSizeSpin->setValue(settings.getAlsaPeriodsize());
    ui.periodsSpin->setValue(settings.getAlsaPeriods());

    ui.ringBufferSpin->setValue(settings.getRingBufSize() / 1024);
    ui.meterRangeSpin->setValue(settings.getMeterRange());

    updateEnabled();
}

void SettingsDialog::updateEnabled()
{
    bool isAlsa = ui.driverComboBox->currentIndex() == 0;
    ui.alsaDeviceComboBox->setEnabled(isAlsa);
    ui.rateSpin->setEnabled(isAlsa);
    ui.periodSizeSpin->setEnabled(isAlsa);
    ui.periodsSpin->setEnabled(isAlsa);

    bool midi = ui.midiTriggerEnabledCheckBox->isChecked();
    ui.midiChannelSpin->setEnabled(midi);
    ui.midiNoteSpin->setEnabled(midi);
}

void SettingsDialog::resetToDefault()
{
    SettingsData def;
    def.validateSettings();
    loadFromSettings(def);
}


void SettingsDialog::loadFromFile()
{
    SettingsData def;
    def.load();
    def.validateSettings();
    loadFromSettings(def);
}

void SettingsDialog::saveToSettings(SettingsData& settings)
{
    settings.setChannels(ui.channelsSpin->value());
    settings.setEnable32bit(ui.bitComboBox->currentIndex() == 1);
    settings.setSplitMB(ui.splitSpin->value());

    settings.setEnableMidiTrigger(ui.midiTriggerEnabledCheckBox->isChecked());
    settings.setMidiChannel(ui.midiChannelSpin->value());
    settings.setMidiNote(ui.midiNoteSpin->value());

    settings.setEnableJack(ui.driverComboBox->currentIndex() == 1);
    settings.setAlsaPcmName(ui.alsaDeviceComboBox->currentText());
    settings.setRate(ui.rateSpin->value());
    settings.setAlsaPeriodsize(ui.periodSizeSpin->value());
    settings.setAlsaPeriods(ui.periodsSpin->value());

    settings.setRingBufSize(ui.ringBufferSpin->value() * 1024);
    settings.setMeterRange(ui.meterRangeSpin->value());

    settings.validateSettings();
}

void SettingsDialog::saveToFile()
{
    SettingsData s;
    saveToSettings(s);
    s.save();
}


void SettingsDialogBase::setupUi ( QDialog *SettingsDialogBase )
{
    if ( SettingsDialogBase->objectName().isEmpty() )
        SettingsDialogBase->setObjectName ( QString::fromUtf8 ( "SettingsDialogBase" ) );
    SettingsDialogBase->resize ( 661, 326 );
    SettingsDialogBase->setModal ( true );
    gridLayout_5 = new QGridLayout ( SettingsDialogBase );
    gridLayout_5->setObjectName ( QString::fromUtf8 ( "gridLayout_5" ) );
    topLayout = new QHBoxLayout();
    topLayout->setObjectName ( QString::fromUtf8 ( "topLayout" ) );
    topLayout->setSizeConstraint ( QLayout::SetDefaultConstraint );
    leftOptionsLayout = new QVBoxLayout();
    leftOptionsLayout->setObjectName ( QString::fromUtf8 ( "leftOptionsLayout" ) );
    formatGroupBox = new QGroupBox ( SettingsDialogBase );
    formatGroupBox->setObjectName ( QString::fromUtf8 ( "formatGroupBox" ) );
    gridLayout_2 = new QGridLayout ( formatGroupBox );
    gridLayout_2->setObjectName ( QString::fromUtf8 ( "gridLayout_2" ) );
    splitLabel = new QLabel ( formatGroupBox );
    splitLabel->setObjectName ( QString::fromUtf8 ( "splitLabel" ) );

    gridLayout_2->addWidget ( splitLabel, 2, 0, 1, 1 );

    channelsSpin = new QSpinBox ( formatGroupBox );
    channelsSpin->setObjectName ( QString::fromUtf8 ( "channelsSpin" ) );
    channelsSpin->setMinimum ( 1 );
    channelsSpin->setMaximum ( 99 );
    channelsSpin->setValue ( 2 );

    gridLayout_2->addWidget ( channelsSpin, 0, 1, 1, 1 );

    bitComboBox = new QComboBox ( formatGroupBox );
    bitComboBox->setObjectName ( QString::fromUtf8 ( "bitComboBox" ) );

    gridLayout_2->addWidget ( bitComboBox, 1, 1, 1, 1 );

    channelsLabel = new QLabel ( formatGroupBox );
    channelsLabel->setObjectName ( QString::fromUtf8 ( "channelsLabel" ) );

    gridLayout_2->addWidget ( channelsLabel, 0, 0, 1, 1 );

    splitSpin = new QSpinBox ( formatGroupBox );
    splitSpin->setObjectName ( QString::fromUtf8 ( "splitSpin" ) );
    splitSpin->setMinimum ( 1 );
    splitSpin->setMaximum ( 4096 );
    splitSpin->setValue ( 2 );

    gridLayout_2->addWidget ( splitSpin, 2, 1, 1, 1 );

    bitLabel = new QLabel ( formatGroupBox );
    bitLabel->setObjectName ( QString::fromUtf8 ( "bitLabel" ) );

    gridLayout_2->addWidget ( bitLabel, 1, 0, 1, 1 );

    splitLabel->raise();
    channelsSpin->raise();
    bitComboBox->raise();
    channelsLabel->raise();
    splitSpin->raise();
    bitLabel->raise();

    leftOptionsLayout->addWidget ( formatGroupBox );

    midiTriggerGroupBox = new QGroupBox ( SettingsDialogBase );
    midiTriggerGroupBox->setObjectName ( QString::fromUtf8 ( "midiTriggerGroupBox" ) );
    verticalLayout_3 = new QVBoxLayout ( midiTriggerGroupBox );
    verticalLayout_3->setObjectName ( QString::fromUtf8 ( "verticalLayout_3" ) );
    midiTriggerEnabledCheckBox = new QCheckBox ( midiTriggerGroupBox );
    midiTriggerEnabledCheckBox->setObjectName ( QString::fromUtf8 ( "midiTriggerEnabledCheckBox" ) );
    midiTriggerEnabledCheckBox->setChecked ( true );

    verticalLayout_3->addWidget ( midiTriggerEnabledCheckBox );

    gridLayout = new QGridLayout();
    gridLayout->setObjectName ( QString::fromUtf8 ( "gridLayout" ) );
    midiChannelLabel = new QLabel ( midiTriggerGroupBox );
    midiChannelLabel->setObjectName ( QString::fromUtf8 ( "midiChannelLabel" ) );

    gridLayout->addWidget ( midiChannelLabel, 0, 0, 1, 1 );

    midiChannelSpin = new QSpinBox ( midiTriggerGroupBox );
    midiChannelSpin->setObjectName ( QString::fromUtf8 ( "midiChannelSpin" ) );
    midiChannelSpin->setMinimum ( 1 );
    midiChannelSpin->setMaximum ( 16 );
    midiChannelSpin->setValue ( 2 );

    gridLayout->addWidget ( midiChannelSpin, 0, 1, 1, 1 );

    midiNoteLabel = new QLabel ( midiTriggerGroupBox );
    midiNoteLabel->setObjectName ( QString::fromUtf8 ( "midiNoteLabel" ) );

    gridLayout->addWidget ( midiNoteLabel, 1, 0, 1, 1 );

    midiNoteSpin = new QSpinBox ( midiTriggerGroupBox );
    midiNoteSpin->setObjectName ( QString::fromUtf8 ( "midiNoteSpin" ) );
    midiNoteSpin->setMinimum ( 0 );
    midiNoteSpin->setMaximum ( 127 );
    midiNoteSpin->setValue ( 2 );

    gridLayout->addWidget ( midiNoteSpin, 1, 1, 1, 1 );


    verticalLayout_3->addLayout ( gridLayout );


    leftOptionsLayout->addWidget ( midiTriggerGroupBox );


    topLayout->addLayout ( leftOptionsLayout );

    inputGroupBox = new QGroupBox ( SettingsDialogBase );
    inputGroupBox->setObjectName ( QString::fromUtf8 ( "inputGroupBox" ) );
    QSizePolicy sizePolicy ( QSizePolicy::Preferred, QSizePolicy::Preferred );
    sizePolicy.setHorizontalStretch ( 0 );
    sizePolicy.setVerticalStretch ( 0 );
    sizePolicy.setHeightForWidth ( inputGroupBox->sizePolicy().hasHeightForWidth() );
    inputGroupBox->setSizePolicy ( sizePolicy );
    verticalLayout_2 = new QVBoxLayout ( inputGroupBox );
    verticalLayout_2->setObjectName ( QString::fromUtf8 ( "verticalLayout_2" ) );
    horizontalLayout = new QHBoxLayout();
    horizontalLayout->setObjectName ( QString::fromUtf8 ( "horizontalLayout" ) );
    driverLabel = new QLabel ( inputGroupBox );
    driverLabel->setObjectName ( QString::fromUtf8 ( "driverLabel" ) );

    horizontalLayout->addWidget ( driverLabel );

    driverComboBox = new QComboBox ( inputGroupBox );
    driverComboBox->setObjectName ( QString::fromUtf8 ( "driverComboBox" ) );

    horizontalLayout->addWidget ( driverComboBox );


    verticalLayout_2->addLayout ( horizontalLayout );

    alsaGroupBox = new QGroupBox ( inputGroupBox );
    alsaGroupBox->setObjectName ( QString::fromUtf8 ( "alsaGroupBox" ) );
    gridLayout_3 = new QGridLayout ( alsaGroupBox );
    gridLayout_3->setObjectName ( QString::fromUtf8 ( "gridLayout_3" ) );
    alsaDeviceLabel = new QLabel ( alsaGroupBox );
    alsaDeviceLabel->setObjectName ( QString::fromUtf8 ( "alsaDeviceLabel" ) );

    gridLayout_3->addWidget ( alsaDeviceLabel, 0, 0, 1, 1 );

    rateLabel = new QLabel ( alsaGroupBox );
    rateLabel->setObjectName ( QString::fromUtf8 ( "rateLabel" ) );

    gridLayout_3->addWidget ( rateLabel, 1, 0, 1, 1 );

    rateSpin = new QSpinBox ( alsaGroupBox );
    rateSpin->setObjectName ( QString::fromUtf8 ( "rateSpin" ) );
    rateSpin->setMinimum ( 100 );
    rateSpin->setMaximum ( 10000000 );
    rateSpin->setValue ( 100 );

    gridLayout_3->addWidget ( rateSpin, 1, 1, 1, 1 );

    periodSizeLabel = new QLabel ( alsaGroupBox );
    periodSizeLabel->setObjectName ( QString::fromUtf8 ( "periodSizeLabel" ) );

    gridLayout_3->addWidget ( periodSizeLabel, 2, 0, 1, 1 );

    periodSizeSpin = new QSpinBox ( alsaGroupBox );
    periodSizeSpin->setObjectName ( QString::fromUtf8 ( "periodSizeSpin" ) );
    periodSizeSpin->setMinimum ( 1 );
    periodSizeSpin->setMaximum ( 9999999 );
    periodSizeSpin->setValue ( 2 );

    gridLayout_3->addWidget ( periodSizeSpin, 2, 1, 1, 1 );

    periodsLabel = new QLabel ( alsaGroupBox );
    periodsLabel->setObjectName ( QString::fromUtf8 ( "periodsLabel" ) );

    gridLayout_3->addWidget ( periodsLabel, 3, 0, 1, 1 );

    periodsSpin = new QSpinBox ( alsaGroupBox );
    periodsSpin->setObjectName ( QString::fromUtf8 ( "periodsSpin" ) );
    periodsSpin->setMinimum ( 1 );
    periodsSpin->setValue ( 2 );

    gridLayout_3->addWidget ( periodsSpin, 3, 1, 1, 1 );

    alsaDeviceComboBox = new QComboBox ( alsaGroupBox );
    alsaDeviceComboBox->setObjectName ( QString::fromUtf8 ( "alsaDeviceComboBox" ) );
    QSizePolicy sizePolicy1 ( QSizePolicy::Expanding, QSizePolicy::Fixed );
    sizePolicy1.setHorizontalStretch ( 0 );
    sizePolicy1.setVerticalStretch ( 0 );
    sizePolicy1.setHeightForWidth ( alsaDeviceComboBox->sizePolicy().hasHeightForWidth() );
    alsaDeviceComboBox->setSizePolicy ( sizePolicy1 );
    alsaDeviceComboBox->setEditable ( true );

    gridLayout_3->addWidget ( alsaDeviceComboBox, 0, 1, 1, 1 );


    verticalLayout_2->addWidget ( alsaGroupBox );

    gridLayout_4 = new QGridLayout();
    gridLayout_4->setObjectName ( QString::fromUtf8 ( "gridLayout_4" ) );
    ringBufferLabel = new QLabel ( inputGroupBox );
    ringBufferLabel->setObjectName ( QString::fromUtf8 ( "ringBufferLabel" ) );

    gridLayout_4->addWidget ( ringBufferLabel, 0, 0, 1, 1 );

    ringBufferSpin = new QSpinBox ( inputGroupBox );
    ringBufferSpin->setObjectName ( QString::fromUtf8 ( "ringBufferSpin" ) );
    ringBufferSpin->setMinimum ( 1 );
    ringBufferSpin->setMaximum ( 999999 );
    ringBufferSpin->setValue ( 2 );

    gridLayout_4->addWidget ( ringBufferSpin, 0, 1, 1, 1 );

    meterRangeLabel = new QLabel ( inputGroupBox );
    meterRangeLabel->setObjectName ( QString::fromUtf8 ( "meterRangeLabel" ) );

    gridLayout_4->addWidget ( meterRangeLabel, 1, 0, 1, 1 );

    meterRangeSpin = new QSpinBox ( inputGroupBox );
    meterRangeSpin->setObjectName ( QString::fromUtf8 ( "meterRangeSpin" ) );
    meterRangeSpin->setMinimum ( 6 );
    meterRangeSpin->setMaximum ( 144 );
    meterRangeSpin->setValue ( 6 );

    gridLayout_4->addWidget ( meterRangeSpin, 1, 1, 1, 1 );


    verticalLayout_2->addLayout ( gridLayout_4 );


    topLayout->addWidget ( inputGroupBox );


    gridLayout_5->addLayout ( topLayout, 0, 0, 1, 1 );

    rightSpacerWidget = new QWidget ( SettingsDialogBase );
    rightSpacerWidget->setObjectName ( QString::fromUtf8 ( "rightSpacerWidget" ) );
    QSizePolicy sizePolicy2 ( QSizePolicy::Fixed, QSizePolicy::Preferred );
    sizePolicy2.setHorizontalStretch ( 0 );
    sizePolicy2.setVerticalStretch ( 0 );
    sizePolicy2.setHeightForWidth ( rightSpacerWidget->sizePolicy().hasHeightForWidth() );
    rightSpacerWidget->setSizePolicy ( sizePolicy2 );
    verticalLayout = new QVBoxLayout ( rightSpacerWidget );
    verticalLayout->setObjectName ( QString::fromUtf8 ( "verticalLayout" ) );
    verticalLayout->setSizeConstraint ( QLayout::SetDefaultConstraint );
    resetButton = new QPushButton ( rightSpacerWidget );
    resetButton->setObjectName ( QString::fromUtf8 ( "resetButton" ) );
    QSizePolicy sizePolicy3 ( QSizePolicy::Minimum, QSizePolicy::Fixed );
    sizePolicy3.setHorizontalStretch ( 0 );
    sizePolicy3.setVerticalStretch ( 0 );
    sizePolicy3.setHeightForWidth ( resetButton->sizePolicy().hasHeightForWidth() );
    resetButton->setSizePolicy ( sizePolicy3 );

    verticalLayout->addWidget ( resetButton );

    loadButton = new QPushButton ( rightSpacerWidget );
    loadButton->setObjectName ( QString::fromUtf8 ( "loadButton" ) );

    verticalLayout->addWidget ( loadButton );

    saveButton = new QPushButton ( rightSpacerWidget );
    saveButton->setObjectName ( QString::fromUtf8 ( "saveButton" ) );
    sizePolicy3.setHeightForWidth ( saveButton->sizePolicy().hasHeightForWidth() );
    saveButton->setSizePolicy ( sizePolicy3 );

    verticalLayout->addWidget ( saveButton );

    spacerWidget = new QWidget ( rightSpacerWidget );
    spacerWidget->setObjectName ( QString::fromUtf8 ( "spacerWidget" ) );
    sizePolicy.setHeightForWidth ( spacerWidget->sizePolicy().hasHeightForWidth() );
    spacerWidget->setSizePolicy ( sizePolicy );

    verticalLayout->addWidget ( spacerWidget );

    buttonBox = new QDialogButtonBox ( rightSpacerWidget );
    buttonBox->setObjectName ( QString::fromUtf8 ( "buttonBox" ) );
    buttonBox->setOrientation ( Qt::Horizontal );
    buttonBox->setStandardButtons ( QDialogButtonBox::Cancel|QDialogButtonBox::Ok );
    buttonBox->setCenterButtons ( true );

    verticalLayout->addWidget ( buttonBox );


    gridLayout_5->addWidget ( rightSpacerWidget, 0, 1, 1, 1 );

#ifndef QT_NO_SHORTCUT
    splitLabel->setBuddy ( splitSpin );
    channelsLabel->setBuddy ( channelsSpin );
    bitLabel->setBuddy ( bitComboBox );
    midiChannelLabel->setBuddy ( midiChannelSpin );
    midiNoteLabel->setBuddy ( midiNoteSpin );
    driverLabel->setBuddy ( driverComboBox );
    alsaDeviceLabel->setBuddy ( alsaDeviceComboBox );
    rateLabel->setBuddy ( rateSpin );
    periodSizeLabel->setBuddy ( periodSizeSpin );
    periodsLabel->setBuddy ( periodsSpin );
    ringBufferLabel->setBuddy ( ringBufferSpin );
    meterRangeLabel->setBuddy ( meterRangeSpin );
#endif // QT_NO_SHORTCUT
    QWidget::setTabOrder ( channelsSpin, bitComboBox );
    QWidget::setTabOrder ( bitComboBox, splitSpin );
    QWidget::setTabOrder ( splitSpin, midiTriggerEnabledCheckBox );
    QWidget::setTabOrder ( midiTriggerEnabledCheckBox, midiChannelSpin );
    QWidget::setTabOrder ( midiChannelSpin, midiNoteSpin );
    QWidget::setTabOrder ( midiNoteSpin, driverComboBox );
    QWidget::setTabOrder ( driverComboBox, alsaDeviceComboBox );
    QWidget::setTabOrder ( alsaDeviceComboBox, rateSpin );
    QWidget::setTabOrder ( rateSpin, periodSizeSpin );
    QWidget::setTabOrder ( periodSizeSpin, periodsSpin );
    QWidget::setTabOrder ( periodsSpin, ringBufferSpin );
    QWidget::setTabOrder ( ringBufferSpin, meterRangeSpin );
    QWidget::setTabOrder ( meterRangeSpin, resetButton );
    QWidget::setTabOrder ( resetButton, loadButton );
    QWidget::setTabOrder ( loadButton, saveButton );
    QWidget::setTabOrder ( saveButton, buttonBox );

    retranslateUi ( SettingsDialogBase );
    QObject::connect(buttonBox, SIGNAL(accepted()), SettingsDialogBase, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), SettingsDialogBase, SLOT(reject()));

    bitComboBox->setCurrentIndex ( 0 );

    QMetaObject::connectSlotsByName ( SettingsDialogBase );
} // setupUi

void SettingsDialogBase::retranslateUi ( QDialog *SettingsDialogBase )
{
    SettingsDialogBase->setWindowTitle(tr("Preferences"));
    formatGroupBox->setTitle (tr("Output format"));
    splitLabel->setText (tr("File &split (MB)"));
    bitComboBox->clear();
    bitComboBox->insertItems ( 0, QStringList()
                               << tr("16 bit")
                               << tr("32 bit")
                             );
    channelsLabel->setText (tr("C&hannels"));
    bitLabel->setText (tr("&Bit depth"));
    midiTriggerGroupBox->setTitle (tr("MIDI trigger"));
    midiTriggerEnabledCheckBox->setText (tr("En&abled"));
    midiChannelLabel->setText (tr("M&IDI Channel"));
    midiNoteLabel->setText (tr("No&te number"));
    inputGroupBox->setTitle (tr("Input"));
    driverLabel->setText (tr("&Driver"));
    driverComboBox->clear();
    driverComboBox->insertItems ( 0, QStringList()
                                  << tr("ALSA")
                                  << tr("JACK")
                                );
    alsaGroupBox->setTitle (tr("ALSA options"));
    alsaDeviceLabel->setText (tr("D&evice name"));
    rateLabel->setText (tr("Sample &rate"));
    periodSizeLabel->setText (tr("&Period size"));
    periodsLabel->setText (tr("&Fragments"));
    ringBufferLabel->setText (tr("Rin&gbuffer size (kB)"));
    meterRangeLabel->setText (tr("Peak &meter range (dB)"));
    resetButton->setText (tr("&Load factory defaults"));
    loadButton->setText (tr("Load start&up defaults"));
    saveButton->setText (tr("Sa&ve as startup default"));
    Q_UNUSED (SettingsDialogBase);
} // retranslateUi
