/* wavtool.c */
/* compile with: gcc -Wall wavtool.c -o wavtool */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

void display_header(char *fn);
void trim32(char *fn, int thrs, int pad_start, int pad_end,
        int fade_start, int fade_end);

int main(int argc, char **argv) {

    if (argc < 7) {
        fprintf(stderr, "\nwavtool <file> <thrs> <pad_start> <pad_end>"
                " <fade_start> <fade_end>\n\n");
        exit(1);
    }
    display_header(argv[1]);
    trim32(argv[1], atoi(argv[2]), atoi(argv[3]), atoi(argv[4]),
            atoi(argv[5]), atoi(argv[6]));
    return (EXIT_SUCCESS);
}

void display_header(char *fn) {

    FILE *f;
    char ChunkID[5], Format[5], SubChunk1ID[5], SubChunk2ID[5];
    int ChunkSize, Subchunk1Size, AudioFormat, NumChannels, SampleRate,
        ByteRate, BlockAlign, BitsPerSample, Subchunk2Size;
    unsigned char c[4];

    f = fopen(fn, "r");
    fread(ChunkID, 1, 4, f);
    ChunkID[4] = 0;
    printf("\nChunkID......: %s\n", ChunkID);
    fread(c, 1, 4, f);
    ChunkSize = c[0] | (c[1] << 8) | (c[2] << 16) | (c[3] << 24);
    printf("ChunkSize....: %d\n", ChunkSize);
    fread(Format, 1, 4, f);
    Format[4] = 0;
    printf("Format.......: %s\n", Format);
    fread(SubChunk1ID, 1, 4, f);
    SubChunk1ID[4] = 0;
    printf("SubChunk1ID..: %s\n", SubChunk1ID);
    fread(c, 1, 4, f);
    Subchunk1Size = c[0] | (c[1] << 8) | (c[2] << 16) | (c[3] << 24);
    printf("Subchunk1Size: %d\n", Subchunk1Size);
    fread(c, 1, 2, f);
    AudioFormat = c[0] | (c[1] << 8);
    printf("AudioFormat..: %d\n", AudioFormat);
    fread(c, 1, 2, f);
    NumChannels = c[0] | (c[1] << 8);
    printf("NumChannels..: %d\n", NumChannels);
    fread(c, 1, 4, f);
    SampleRate = c[0] | (c[1] << 8) | (c[2] << 16) | (c[3] << 24);
    printf("SampleRate...: %d\n", SampleRate);
    fread(c, 1, 4, f);
    ByteRate = c[0] | (c[1] << 8) | (c[2] << 16) | (c[3] << 24);
    printf("ByteRate.....: %d\n", ByteRate);
    fread(c, 1, 2, f);
    BlockAlign = c[0] | (c[1] << 8);
    printf("BlockAlign...: %d\n", BlockAlign);
    fread(c, 1, 2, f);
    BitsPerSample = c[0] | (c[1] << 8);
    printf("BitsPerSample: %d\n", BitsPerSample);
    fread(SubChunk2ID, 1, 4, f);
    SubChunk2ID[4] = 0;
    printf("SubChunk2ID..: %s\n", SubChunk2ID);
    fread(c, 1, 4, f);
    Subchunk2Size = c[0] | (c[1] << 8) | (c[2] << 16) | (c[3] << 24);
    printf("Subchunk2Size: %d\n", Subchunk2Size);
    fclose(f);
}

void trim32(char *fn, int thrs, int pad_start, int pad_end,
        int fade_start, int fade_end) {

    FILE *f, *fw;
    char *fnw;
    int l1, l2, index, index1, index2, i[2], max[2], size1, size2, gate, trans;
    unsigned char c[4];
    double val, df1, df2, fader1, fader2;

    f = fopen(fn, "r");
    fnw = (char *)malloc(strlen(fn) + 11);
    strcpy (fnw, fn);
    fnw = strcat (fnw, "_trim.wav");
    if (!(fw = fopen(fnw, "w"))) {
        fprintf(stderr, "Could not open %s for writing.\n", fnw);
    } else {
        printf("Writing to %s.\n", fnw);
    }
    fseek(f, 40, SEEK_SET);
    fread(c, 1, 4, f);
    size1 = c[0] | (c[1] << 8) | (c[2] << 16) | (c[3] << 24);
    size1 /= 8;
    printf("\nSize1: %d\n", size1);
    index = 0;
    index1 = 0;
    index2 = 0;
    gate = 0;
    trans = 0;
    for (l1 = 0; l1 < 2; l1++) {
        max[l1] = 0;
    }
    while (!feof(f) && index < size1) {
        for (l2 = 0; l2 < 2; l2++) {
            fread(c, 1, 4, f);
            i[l2] = c[0] | (c[1] << 8) | (c[2] << 16) | (c[3] << 24);
            if (abs(i[l2]) > max[l2]) {
                max[l2] = abs(i[l2]);
            }
        }  
        if (abs(i[0]) + abs(i[1]) > thrs) {
            if (!index1) {
                index1 = index;
            }  
            trans = (gate) ? 0 : 1;
            gate = 1;
        } else {
            trans = (gate) ? 1 : 0;
            gate = 0;
        }
        if (trans && !gate && index1) {
            index2 = index;
        }
        index++;
    }
    if (!index2) {
        index2 = size1;
    }
    printf("Max1: %d\n", max[0]);
    printf("Max2: %d\n", max[1]);
    printf("Index1: %d\n", index1);
    printf("Index2: %d\n", index2);
    index1 -= pad_start;
    if (index1 < 0) index1 = 0;
    else if (index1 > index2) index1 = index2;
    index2 += pad_end;
    if (index2 < index1) index2 = index1;
    size2 = index2 - index1;
    rewind(f);
    fread(c, 1, 4, f);
    fwrite(c, 1, 4, fw); 
    i[0] = (size2 * 8) + 36;
    c[3] = i[0] >> 24;  // ChunkSize
    c[2] = (i[0] >> 16) - ((i[0] >> 24) << 8);
    c[1] = (i[0] >> 8) - ((i[0] >> 16) << 8);
    c[0] = (unsigned char)i[0];
    fwrite(c, 1, 4, fw);  
    fseek(f, 8, SEEK_SET);
    for (l1 = 0; l1 < 32; l1++) {
        c[0] = (unsigned char)fgetc(f);
        fputc(c[0], fw);    
    }
    c[3] = (size2 * 8) >> 24;  // Subchunk2Size
    c[2] = ((size2 * 8) >> 16) - (((size2 * 8) >> 24) << 8);
    c[1] = ((size2 * 8) >> 8) - (((size2 * 8) >> 16) << 8);   
    c[0] = (unsigned char)(size2 * 8);
    fwrite(c, 1, 4, fw);
    fseek(f, 44 + 8 * index1, SEEK_SET);
    if (fade_start + fade_end > size2) {
        fade_start = size2 >> 2;
        fade_end = size2 >> 2;
    }
    df1 = (fade_start > 0) ? 1.0 / (double)fade_start : 0.0;
    df2 = (fade_end > 1) ? 1.0 / (double)(fade_end - 1.0) : 0.0;
    fader1 = 0;
    fader2 = 1.0; 
    for (l1 = 0; l1 < fade_start; l1++) { 
        for (l2 = 0; l2 < 2; l2++) {
            if (!feof(f)) {
                fread(c, 1, 4, f);
                i[l2] = c[0] | (c[1] << 8) | (c[2] << 16) | (c[3] << 24);
            } else {
                i[l2] = 0;
            }
            val = fader1 * fader1 * fader1 * (double)i[l2];
            i[l2] = (int)val;
            c[3] = i[l2] >> 24; 
            c[2] = (i[l2] >> 16) - ((i[l2] >> 24) << 8);
            c[1] = (i[l2] >> 8) - ((i[l2] >> 16) << 8);
            c[0] = (unsigned char)i[l2];
            fwrite(c, 1, 4, fw);
        }
        fader1 += df1;
    }    
    for (l1 = fade_start; l1 < size2 - fade_end; l1++) { 
        for (l2 = 0; l2 < 2; l2++) {
            if (!feof(f)) {
                fread(c, 1, 4, f);
            } else {
                bzero(c, 4);
            }
            fwrite(c, 1, 4, fw);
        }
    }    
    for (l1 = size2 - fade_end; l1 < size2; l1++) { 
        for (l2 = 0; l2 < 2; l2++) {
            if (!feof(f)) {
                fread(c, 1, 4, f);
                i[l2] = c[0] | (c[1] << 8) | (c[2] << 16) | (c[3] << 24);
            } else {
                i[l2] = 0;
            }
            val = fader2 * fader2 * fader2 * (double)i[l2];
            //      fprintf(stderr, "l1: %d l2: %d i: %d, fader2: %f val: %f\n", l1, l2, i[l2], fader2, val); 
            i[l2] = (int)val;
            c[3] = i[l2] >> 24;
            c[2] = (i[l2] >> 16) - ((i[l2] >> 24) << 8);
            c[1] = (i[l2] >> 8) - ((i[l2] >> 16) << 8); 
            c[0] = (unsigned char)i[l2];
            fwrite(c, 1, 4, fw);
        }
        fader2 -= df2;
    }    
    fclose(f);
    fclose(fw);          
}
