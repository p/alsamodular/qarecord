#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QString>
#include <QTextStream>

class SettingsData : public QObject
{
  private:
// General options
    bool enable32bit;
    unsigned long ringBufSize;
    int channels;
    bool enableJack;
    int meterRange;
    int splitMB;

// ALSA options
    int alsaPeriods;
    int alsaPeriodsize;
    QString alsaPcmName;
    int rate; // Set to JACK samplerate if JACK is used

// Midi trigger options
    bool disableMidiTrigger;
    int midiChannel;
    int midiNote;

    void printHelp(QTextStream&);

  public:
    SettingsData();
    void setDefault();
    /**
     * @return 0 if the program should exit (--help and --version options),
     * nonzero otherwise
     */
    int parseCmdLine(int argc, char *argv[]);

    void load();
    void save();

    void validateSettings();

    // Property accessors
    unsigned long getRingBufSize() { return ringBufSize; }
    void setRingBufSize(unsigned long r) { ringBufSize = r; }
    int getRate() const { return rate; }
    void setRate(int r) { rate = r; } 
    int getChannels() const { return channels; }
    void setChannels(int c) { channels = c; }
    bool getEnableJack() const { return enableJack; }
    void setEnableJack(bool b) { enableJack = b; }
    bool getEnable32bit() const { return enable32bit; }
    void setEnable32bit(bool b) { enable32bit = b; }
    bool getEnableMidiTrigger() const { return !disableMidiTrigger; }
    void setEnableMidiTrigger(bool b) { disableMidiTrigger = !b; }
    int getMidiChannel() const { return midiChannel; }
    void setMidiChannel(int c) { midiChannel = c; }
    int getMidiNote() const { return midiNote; }
    void setMidiNote(int c) { midiNote = c; }
    int getFrameSize() const { return channels * (enable32bit ? 4 : 2); }
    int getSampleSize() const { return (enable32bit ? 4 : 2); }
    int getMeterRange() const { return meterRange; }
    void setMeterRange(int c) { meterRange = c; }
    int getSplitMB() const { return splitMB; }
    void setSplitMB(int c) { splitMB = c; }

    int getAlsaPeriods() const { return alsaPeriods; }
    void setAlsaPeriods(int c) { alsaPeriods = c; }
    int getAlsaPeriodsize() const { return alsaPeriodsize; }
    void setAlsaPeriodsize(int c) { alsaPeriodsize = c; }
    QString getAlsaPcmName() const { return alsaPcmName; }
    void setAlsaPcmName(QString c) { alsaPcmName = c; }
};


#endif
