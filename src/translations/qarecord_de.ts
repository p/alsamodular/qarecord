<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="66"/>
        <location filename="../mainwindow.cpp" line="316"/>
        <source>Stopped</source>
        <translation>Angehalten</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="211"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="211"/>
        <source>L</source>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="79"/>
        <source>&amp;Record</source>
        <translation>&amp;Aufnahme</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="80"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="81"/>
        <source>&amp;Stop</source>
        <translation>&amp;Stopp</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="108"/>
        <source>Peak:</source>
        <translation>Spitze:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="135"/>
        <source>&amp;Preferences...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="268"/>
        <source>About %1</source>
        <translation>Über %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="303"/>
        <source>Recording</source>
        <translation>Aufnahme</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="330"/>
        <source>Paused</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="346"/>
        <source>File: %1</source>
        <translation>Datei: %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="441"/>
        <source>Stop recording?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Time: 0:00:00</source>
        <translation type="obsolete">Zeit: 0:00:00</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="372"/>
        <source>Time: </source>
        <translation>Zeit: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="388"/>
        <source>Buffer overflow</source>
        <translation>Pufferüberlauf</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="43"/>
        <source>File:</source>
        <translation>Datei:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="55"/>
        <source>&amp;Capture</source>
        <translation>&amp;Einfangen</translation>
    </message>
    <message>
        <source>Time: 0:00:00  </source>
        <translation type="obsolete">Zeit: 0:00:00  </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="339"/>
        <source>Choose file name</source>
        <translation>Dateiname wählen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="21"/>
        <source>WAV files (*.wav)</source>
        <translation>WAV-Dateien (*.wav)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="273"/>
        <source>About Qt</source>
        <translation>Über Qt</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="96"/>
        <source>Buffer fill rate</source>
        <translation>Pufferfüllgrad</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="101"/>
        <source>Current:</source>
        <translation>Aktuell:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="372"/>
        <source>H:mm:ss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="385"/>
        <source>%1 % (%2 bytes)</source>
        <translation>%1 % (%2 Byte)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="381"/>
        <source>%1 %</source>
        <translation>%1 %</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="132"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="133"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="134"/>
        <source>&amp;New...</source>
        <translation>&amp;Neu...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="136"/>
        <source>&amp;Quit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="137"/>
        <source>Ctrl+Q</source>
        <comment>File|Quit</comment>
        <translation>Strg+Q</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="138"/>
        <source>&amp;About %1...</source>
        <translation>&amp;Über %1...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="140"/>
        <source>&amp;About Qt...</source>
        <translation>Über &amp;Qt...</translation>
    </message>
</context>
<context>
    <name>SettingsDialogBase</name>
    <message>
        <location filename="../settingsdialog.cpp" line="478"/>
        <location filename="../ui_SettingsDialogBase.h" line="373"/>
        <source>Output format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="485"/>
        <location filename="../ui_SettingsDialogBase.h" line="374"/>
        <source>C&amp;hannels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="486"/>
        <location filename="../ui_SettingsDialogBase.h" line="375"/>
        <source>&amp;Bit depth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="482"/>
        <location filename="../ui_SettingsDialogBase.h" line="378"/>
        <source>16 bit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="483"/>
        <location filename="../ui_SettingsDialogBase.h" line="379"/>
        <source>32 bit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="479"/>
        <location filename="../ui_SettingsDialogBase.h" line="381"/>
        <source>File &amp;split (MB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="477"/>
        <location filename="../ui_SettingsDialogBase.h" line="372"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="487"/>
        <location filename="../ui_SettingsDialogBase.h" line="382"/>
        <source>MIDI trigger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="488"/>
        <location filename="../ui_SettingsDialogBase.h" line="383"/>
        <source>En&amp;abled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="489"/>
        <location filename="../ui_SettingsDialogBase.h" line="384"/>
        <source>M&amp;IDI Channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="490"/>
        <location filename="../ui_SettingsDialogBase.h" line="385"/>
        <source>No&amp;te number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="491"/>
        <location filename="../ui_SettingsDialogBase.h" line="386"/>
        <source>Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="492"/>
        <location filename="../ui_SettingsDialogBase.h" line="387"/>
        <source>&amp;Driver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="495"/>
        <location filename="../ui_SettingsDialogBase.h" line="390"/>
        <source>ALSA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="496"/>
        <location filename="../ui_SettingsDialogBase.h" line="391"/>
        <source>JACK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="498"/>
        <location filename="../ui_SettingsDialogBase.h" line="393"/>
        <source>ALSA options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="499"/>
        <location filename="../ui_SettingsDialogBase.h" line="394"/>
        <source>D&amp;evice name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="500"/>
        <location filename="../ui_SettingsDialogBase.h" line="395"/>
        <source>Sample &amp;rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="505"/>
        <source>&amp;Load factory defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="506"/>
        <source>Load start&amp;up defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="507"/>
        <source>Sa&amp;ve as startup default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="501"/>
        <location filename="../ui_SettingsDialogBase.h" line="396"/>
        <source>&amp;Period size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="502"/>
        <location filename="../ui_SettingsDialogBase.h" line="397"/>
        <source>&amp;Fragments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="503"/>
        <location filename="../ui_SettingsDialogBase.h" line="398"/>
        <source>Rin&amp;gbuffer size (kB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="504"/>
        <location filename="../ui_SettingsDialogBase.h" line="399"/>
        <source>Peak &amp;meter range (dB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui_SettingsDialogBase.h" line="400"/>
        <source>&amp;Load defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui_SettingsDialogBase.h" line="401"/>
        <source>Sa&amp;ve as default</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
