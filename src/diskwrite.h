#ifndef DISKWRITE_H
#define DISKWRITE_H

#include <qobject.h>
#include <qthread.h>
#include <qmutex.h>

#include "ringbuffer.h"
#include "settings.h"

class DiskWrite : public QThread
{

  private:
    RingBuffer *ringBuffer;
    FILE *wavfile;
    int channels, samplesize, framesize, rate, fileIndex;
    bool extendedHeader;
    bool isPaused, doRecord;
    unsigned char *wavdata;
    QString baseFileName, currentFileName; 

    void renameExistingFile();
    int readBlock(bool waitIfEmpty, bool writeToFile);
    long long totalDataSize, currentDataSize, sizeLimit;
    void writeLong(unsigned long i);
    void writeShort(short i);
    void write4CC(const char *c);
    void startFile();
    void finishFile();

  public:
    QMutex mutex; 
    DiskWrite(SettingsData *settings, RingBuffer *p_ringBuffer);
    ~DiskWrite();
    void setFileName(QString s) { baseFileName = s; }
    void setLimit(long long limit) { sizeLimit = limit; }
    virtual void run();
    void setPaused(bool p) { isPaused = p; }
    bool getPaused() { return isPaused; }
    void stop() { doRecord = false; }
    long long getDataSize() { return totalDataSize; }
    long long getCurrentDataSize() { return currentDataSize; }
};
  
#endif
