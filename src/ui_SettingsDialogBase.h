/********************************************************************************
** Form generated from reading ui file 'SettingsDialogBaseF25655.ui'
**
** Created: Sun Sep 6 14:12:00 2009
**      by: Qt User Interface Compiler version 4.5.0
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef SETTINGSDIALOGBASEF25655_H
#define SETTINGSDIALOGBASEF25655_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_SettingsDialogBase
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *topLayout;
    QVBoxLayout *leftOptionsLayout;
    QGroupBox *formatGroupBox;
    QGridLayout *gridLayout_2;
    QLabel *channelsLabel;
    QSpinBox *channelsSpin;
    QLabel *bitLabel;
    QComboBox *bitComboBox;
    QLabel *splitLabel;
    QSpinBox *splitSpin;
    QGroupBox *midiTriggerGroupBox;
    QVBoxLayout *verticalLayout_3;
    QCheckBox *midiTriggerEnabledCheckBox;
    QGridLayout *gridLayout;
    QLabel *midiChannelLabel;
    QSpinBox *midiChannelSpin;
    QLabel *midiNoteLabel;
    QSpinBox *midiNoteSpin;
    QGroupBox *inputGroupBox;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QLabel *driverLabel;
    QComboBox *driverComboBox;
    QGroupBox *alsaGroupBox;
    QGridLayout *gridLayout_3;
    QLabel *alsaDeviceLabel;
    QLineEdit *alsaDeviceEdit;
    QLabel *rateLabel;
    QSpinBox *rateSpin;
    QLabel *periodSizeLabel;
    QSpinBox *periodSizeSpin;
    QLabel *periodsLabel;
    QSpinBox *periodsSpin;
    QGridLayout *gridLayout_4;
    QLabel *ringBufferLabel;
    QSpinBox *ringBufferSpin;
    QLabel *meterRangeLabel;
    QSpinBox *meterRangeSpin;
    QHBoxLayout *bottomButtonsLayout;
    QPushButton *resetButton;
    QPushButton *saveButton;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *SettingsDialogBase)
    {
        if (SettingsDialogBase->objectName().isEmpty())
            SettingsDialogBase->setObjectName(QString::fromUtf8("SettingsDialogBase"));
        SettingsDialogBase->resize(472, 367);
        SettingsDialogBase->setModal(true);
        verticalLayout = new QVBoxLayout(SettingsDialogBase);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        topLayout = new QHBoxLayout();
        topLayout->setObjectName(QString::fromUtf8("topLayout"));
        topLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        leftOptionsLayout = new QVBoxLayout();
        leftOptionsLayout->setObjectName(QString::fromUtf8("leftOptionsLayout"));
        formatGroupBox = new QGroupBox(SettingsDialogBase);
        formatGroupBox->setObjectName(QString::fromUtf8("formatGroupBox"));
        gridLayout_2 = new QGridLayout(formatGroupBox);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        channelsLabel = new QLabel(formatGroupBox);
        channelsLabel->setObjectName(QString::fromUtf8("channelsLabel"));

        gridLayout_2->addWidget(channelsLabel, 0, 0, 1, 1);

        channelsSpin = new QSpinBox(formatGroupBox);
        channelsSpin->setObjectName(QString::fromUtf8("channelsSpin"));
        channelsSpin->setMinimum(1);
        channelsSpin->setMaximum(99);
        channelsSpin->setValue(2);

        gridLayout_2->addWidget(channelsSpin, 0, 1, 1, 1);

        bitLabel = new QLabel(formatGroupBox);
        bitLabel->setObjectName(QString::fromUtf8("bitLabel"));

        gridLayout_2->addWidget(bitLabel, 1, 0, 1, 1);

        bitComboBox = new QComboBox(formatGroupBox);
        bitComboBox->setObjectName(QString::fromUtf8("bitComboBox"));

        gridLayout_2->addWidget(bitComboBox, 1, 1, 1, 1);

        splitLabel = new QLabel(formatGroupBox);
        splitLabel->setObjectName(QString::fromUtf8("splitLabel"));

        gridLayout_2->addWidget(splitLabel, 2, 0, 1, 1);

        splitSpin = new QSpinBox(formatGroupBox);
        splitSpin->setObjectName(QString::fromUtf8("splitSpin"));
        splitSpin->setMinimum(1);
        splitSpin->setMaximum(4096);
        splitSpin->setValue(2);

        gridLayout_2->addWidget(splitSpin, 2, 1, 1, 1);

        splitLabel->raise();
        channelsSpin->raise();
        bitComboBox->raise();
        channelsLabel->raise();
        splitSpin->raise();
        bitLabel->raise();

        leftOptionsLayout->addWidget(formatGroupBox);

        midiTriggerGroupBox = new QGroupBox(SettingsDialogBase);
        midiTriggerGroupBox->setObjectName(QString::fromUtf8("midiTriggerGroupBox"));
        verticalLayout_3 = new QVBoxLayout(midiTriggerGroupBox);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        midiTriggerEnabledCheckBox = new QCheckBox(midiTriggerGroupBox);
        midiTriggerEnabledCheckBox->setObjectName(QString::fromUtf8("midiTriggerEnabledCheckBox"));
        midiTriggerEnabledCheckBox->setChecked(true);

        verticalLayout_3->addWidget(midiTriggerEnabledCheckBox);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        midiChannelLabel = new QLabel(midiTriggerGroupBox);
        midiChannelLabel->setObjectName(QString::fromUtf8("midiChannelLabel"));

        gridLayout->addWidget(midiChannelLabel, 0, 0, 1, 1);

        midiChannelSpin = new QSpinBox(midiTriggerGroupBox);
        midiChannelSpin->setObjectName(QString::fromUtf8("midiChannelSpin"));
        midiChannelSpin->setMinimum(1);
        midiChannelSpin->setMaximum(16);
        midiChannelSpin->setValue(2);

        gridLayout->addWidget(midiChannelSpin, 0, 1, 1, 1);

        midiNoteLabel = new QLabel(midiTriggerGroupBox);
        midiNoteLabel->setObjectName(QString::fromUtf8("midiNoteLabel"));

        gridLayout->addWidget(midiNoteLabel, 1, 0, 1, 1);

        midiNoteSpin = new QSpinBox(midiTriggerGroupBox);
        midiNoteSpin->setObjectName(QString::fromUtf8("midiNoteSpin"));
        midiNoteSpin->setMinimum(0);
        midiNoteSpin->setMaximum(127);
        midiNoteSpin->setValue(2);

        gridLayout->addWidget(midiNoteSpin, 1, 1, 1, 1);


        verticalLayout_3->addLayout(gridLayout);


        leftOptionsLayout->addWidget(midiTriggerGroupBox);


        topLayout->addLayout(leftOptionsLayout);

        inputGroupBox = new QGroupBox(SettingsDialogBase);
        inputGroupBox->setObjectName(QString::fromUtf8("inputGroupBox"));
        verticalLayout_2 = new QVBoxLayout(inputGroupBox);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        driverLabel = new QLabel(inputGroupBox);
        driverLabel->setObjectName(QString::fromUtf8("driverLabel"));

        horizontalLayout->addWidget(driverLabel);

        driverComboBox = new QComboBox(inputGroupBox);
        driverComboBox->setObjectName(QString::fromUtf8("driverComboBox"));

        horizontalLayout->addWidget(driverComboBox);


        verticalLayout_2->addLayout(horizontalLayout);

        alsaGroupBox = new QGroupBox(inputGroupBox);
        alsaGroupBox->setObjectName(QString::fromUtf8("alsaGroupBox"));
        gridLayout_3 = new QGridLayout(alsaGroupBox);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        alsaDeviceLabel = new QLabel(alsaGroupBox);
        alsaDeviceLabel->setObjectName(QString::fromUtf8("alsaDeviceLabel"));

        gridLayout_3->addWidget(alsaDeviceLabel, 0, 0, 1, 1);

        alsaDeviceEdit = new QLineEdit(alsaGroupBox);
        alsaDeviceEdit->setObjectName(QString::fromUtf8("alsaDeviceEdit"));

        gridLayout_3->addWidget(alsaDeviceEdit, 0, 1, 1, 1);

        rateLabel = new QLabel(alsaGroupBox);
        rateLabel->setObjectName(QString::fromUtf8("rateLabel"));

        gridLayout_3->addWidget(rateLabel, 1, 0, 1, 1);

        rateSpin = new QSpinBox(alsaGroupBox);
        rateSpin->setObjectName(QString::fromUtf8("rateSpin"));
        rateSpin->setMinimum(100);
        rateSpin->setMaximum(10000000);
        rateSpin->setValue(100);

        gridLayout_3->addWidget(rateSpin, 1, 1, 1, 1);

        periodSizeLabel = new QLabel(alsaGroupBox);
        periodSizeLabel->setObjectName(QString::fromUtf8("periodSizeLabel"));

        gridLayout_3->addWidget(periodSizeLabel, 2, 0, 1, 1);

        periodSizeSpin = new QSpinBox(alsaGroupBox);
        periodSizeSpin->setObjectName(QString::fromUtf8("periodSizeSpin"));
        periodSizeSpin->setMinimum(1);
        periodSizeSpin->setMaximum(9999999);
        periodSizeSpin->setValue(2);

        gridLayout_3->addWidget(periodSizeSpin, 2, 1, 1, 1);

        periodsLabel = new QLabel(alsaGroupBox);
        periodsLabel->setObjectName(QString::fromUtf8("periodsLabel"));

        gridLayout_3->addWidget(periodsLabel, 3, 0, 1, 1);

        periodsSpin = new QSpinBox(alsaGroupBox);
        periodsSpin->setObjectName(QString::fromUtf8("periodsSpin"));
        periodsSpin->setMinimum(1);
        periodsSpin->setValue(2);

        gridLayout_3->addWidget(periodsSpin, 3, 1, 1, 1);


        verticalLayout_2->addWidget(alsaGroupBox);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        ringBufferLabel = new QLabel(inputGroupBox);
        ringBufferLabel->setObjectName(QString::fromUtf8("ringBufferLabel"));

        gridLayout_4->addWidget(ringBufferLabel, 0, 0, 1, 1);

        ringBufferSpin = new QSpinBox(inputGroupBox);
        ringBufferSpin->setObjectName(QString::fromUtf8("ringBufferSpin"));
        ringBufferSpin->setMinimum(1);
        ringBufferSpin->setMaximum(999999);
        ringBufferSpin->setValue(2);

        gridLayout_4->addWidget(ringBufferSpin, 0, 1, 1, 1);

        meterRangeLabel = new QLabel(inputGroupBox);
        meterRangeLabel->setObjectName(QString::fromUtf8("meterRangeLabel"));

        gridLayout_4->addWidget(meterRangeLabel, 1, 0, 1, 1);

        meterRangeSpin = new QSpinBox(inputGroupBox);
        meterRangeSpin->setObjectName(QString::fromUtf8("meterRangeSpin"));
        meterRangeSpin->setMinimum(6);
        meterRangeSpin->setMaximum(144);
        meterRangeSpin->setValue(6);

        gridLayout_4->addWidget(meterRangeSpin, 1, 1, 1, 1);


        verticalLayout_2->addLayout(gridLayout_4);


        topLayout->addWidget(inputGroupBox);


        verticalLayout->addLayout(topLayout);

        bottomButtonsLayout = new QHBoxLayout();
        bottomButtonsLayout->setObjectName(QString::fromUtf8("bottomButtonsLayout"));
        bottomButtonsLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        resetButton = new QPushButton(SettingsDialogBase);
        resetButton->setObjectName(QString::fromUtf8("resetButton"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(resetButton->sizePolicy().hasHeightForWidth());
        resetButton->setSizePolicy(sizePolicy);

        bottomButtonsLayout->addWidget(resetButton);

        saveButton = new QPushButton(SettingsDialogBase);
        saveButton->setObjectName(QString::fromUtf8("saveButton"));
        sizePolicy.setHeightForWidth(saveButton->sizePolicy().hasHeightForWidth());
        saveButton->setSizePolicy(sizePolicy);

        bottomButtonsLayout->addWidget(saveButton);

        buttonBox = new QDialogButtonBox(SettingsDialogBase);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        bottomButtonsLayout->addWidget(buttonBox);


        verticalLayout->addLayout(bottomButtonsLayout);

#ifndef QT_NO_SHORTCUT
        channelsLabel->setBuddy(channelsSpin);
        bitLabel->setBuddy(bitComboBox);
        splitLabel->setBuddy(splitSpin);
        midiChannelLabel->setBuddy(midiChannelSpin);
        midiNoteLabel->setBuddy(midiNoteSpin);
        driverLabel->setBuddy(driverComboBox);
        alsaDeviceLabel->setBuddy(alsaDeviceEdit);
        rateLabel->setBuddy(rateSpin);
        periodSizeLabel->setBuddy(periodSizeSpin);
        periodsLabel->setBuddy(periodsSpin);
        ringBufferLabel->setBuddy(ringBufferSpin);
        meterRangeLabel->setBuddy(meterRangeSpin);
#endif // QT_NO_SHORTCUT
        QWidget::setTabOrder(channelsSpin, bitComboBox);
        QWidget::setTabOrder(bitComboBox, splitSpin);
        QWidget::setTabOrder(splitSpin, midiTriggerEnabledCheckBox);
        QWidget::setTabOrder(midiTriggerEnabledCheckBox, midiChannelSpin);
        QWidget::setTabOrder(midiChannelSpin, midiNoteSpin);
        QWidget::setTabOrder(midiNoteSpin, driverComboBox);
        QWidget::setTabOrder(driverComboBox, alsaDeviceEdit);
        QWidget::setTabOrder(alsaDeviceEdit, rateSpin);
        QWidget::setTabOrder(rateSpin, periodSizeSpin);
        QWidget::setTabOrder(periodSizeSpin, periodsSpin);
        QWidget::setTabOrder(periodsSpin, ringBufferSpin);
        QWidget::setTabOrder(ringBufferSpin, meterRangeSpin);
        QWidget::setTabOrder(meterRangeSpin, resetButton);
        QWidget::setTabOrder(resetButton, saveButton);
        QWidget::setTabOrder(saveButton, buttonBox);

        retranslateUi(SettingsDialogBase);
        QObject::connect(buttonBox, SIGNAL(accepted()), SettingsDialogBase, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), SettingsDialogBase, SLOT(reject()));

        bitComboBox->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(SettingsDialogBase);
    } // setupUi

    void retranslateUi(QDialog *SettingsDialogBase)
    {
        SettingsDialogBase->setWindowTitle(QApplication::translate("SettingsDialogBase", "Preferences", 0, QApplication::UnicodeUTF8));
        formatGroupBox->setTitle(QApplication::translate("SettingsDialogBase", "Output format", 0, QApplication::UnicodeUTF8));
        channelsLabel->setText(QApplication::translate("SettingsDialogBase", "C&hannels", 0, QApplication::UnicodeUTF8));
        bitLabel->setText(QApplication::translate("SettingsDialogBase", "&Bit depth", 0, QApplication::UnicodeUTF8));
        bitComboBox->clear();
        bitComboBox->insertItems(0, QStringList()
         << QApplication::translate("SettingsDialogBase", "16 bit", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SettingsDialogBase", "32 bit", 0, QApplication::UnicodeUTF8)
        );
        splitLabel->setText(QApplication::translate("SettingsDialogBase", "File &split (MB)", 0, QApplication::UnicodeUTF8));
        midiTriggerGroupBox->setTitle(QApplication::translate("SettingsDialogBase", "MIDI trigger", 0, QApplication::UnicodeUTF8));
        midiTriggerEnabledCheckBox->setText(QApplication::translate("SettingsDialogBase", "En&abled", 0, QApplication::UnicodeUTF8));
        midiChannelLabel->setText(QApplication::translate("SettingsDialogBase", "M&IDI Channel", 0, QApplication::UnicodeUTF8));
        midiNoteLabel->setText(QApplication::translate("SettingsDialogBase", "No&te number", 0, QApplication::UnicodeUTF8));
        inputGroupBox->setTitle(QApplication::translate("SettingsDialogBase", "Input", 0, QApplication::UnicodeUTF8));
        driverLabel->setText(QApplication::translate("SettingsDialogBase", "&Driver", 0, QApplication::UnicodeUTF8));
        driverComboBox->clear();
        driverComboBox->insertItems(0, QStringList()
         << QApplication::translate("SettingsDialogBase", "ALSA", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SettingsDialogBase", "JACK", 0, QApplication::UnicodeUTF8)
        );
        alsaGroupBox->setTitle(QApplication::translate("SettingsDialogBase", "ALSA options", 0, QApplication::UnicodeUTF8));
        alsaDeviceLabel->setText(QApplication::translate("SettingsDialogBase", "D&evice name", 0, QApplication::UnicodeUTF8));
        rateLabel->setText(QApplication::translate("SettingsDialogBase", "Sample &rate", 0, QApplication::UnicodeUTF8));
        periodSizeLabel->setText(QApplication::translate("SettingsDialogBase", "&Period size", 0, QApplication::UnicodeUTF8));
        periodsLabel->setText(QApplication::translate("SettingsDialogBase", "&Fragments", 0, QApplication::UnicodeUTF8));
        ringBufferLabel->setText(QApplication::translate("SettingsDialogBase", "Rin&gbuffer size (kB)", 0, QApplication::UnicodeUTF8));
        meterRangeLabel->setText(QApplication::translate("SettingsDialogBase", "Peak &meter range (dB)", 0, QApplication::UnicodeUTF8));
        resetButton->setText(QApplication::translate("SettingsDialogBase", "&Load defaults", 0, QApplication::UnicodeUTF8));
        saveButton->setText(QApplication::translate("SettingsDialogBase", "Sa&ve as default", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(SettingsDialogBase);
    } // retranslateUi

};

namespace Ui {
    class SettingsDialogBase: public Ui_SettingsDialogBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // SETTINGSDIALOGBASEF25655_H
