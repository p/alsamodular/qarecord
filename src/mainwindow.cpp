#include <QBoxLayout>
#include <QDir>
#include <QGroupBox>
#include <QFileDialog>
#include <QPushButton>
#include <QMenuBar>
#include <QMenu>
#include <QTime>

#include "mainwindow.h"
#include "meter.h"
#include "pixmaps/qarecord_48.xpm"
#include "settingsdialog.h"

static const char WAVEEXT[] = ".wav";
static const char FILEPRESET[] = "/New1.wav";


MainWindow::MainWindow(SettingsData* p_settings)
{
    fileFilter = tr("WAV files (*.wav)");

    diskwrite = NULL;
    jackCapture = NULL;
    alsaCapture = NULL;
    ringBuffer = NULL;
    midiTrigger = NULL;

    settings = p_settings;
    lastDir = QDir::homePath();

    createBaseGUI();
    settingsChanged();

    show();
}

void MainWindow::createBaseGUI()
{
    // file line
    QHBoxLayout *fileBoxLayout = new QHBoxLayout;
    currentFileLabel = new QLabel(this);
    currentFileLabel->setText(tr("File:"));
    fileBoxLayout->setMargin(3);
    fileBoxLayout->setSpacing(50);
    fileBoxLayout->addWidget(currentFileLabel);
    currentFileLabel->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);

    // time/capture line
    QHBoxLayout *timeBoxLayout = new QHBoxLayout;
    timeBoxLayout->setMargin(3);
    timeBoxLayout->setSpacing(50);

    captureToggle = new QCheckBox(this);
    captureToggle->setText(tr("&Capture"));
    QObject::connect(captureToggle, SIGNAL(toggled(bool)),
             this, SLOT(captureToggled(bool)));
    timeBoxLayout->addWidget(captureToggle);

    timeLabel = new QLabel(this);
    //timeLabel->setText(tr("Time: 0:00:00  "));
    timeLabel->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    timeBoxLayout->addWidget(timeLabel);

    statusLabel = new QLabel(this);
    statusLabel->setText(tr("Stopped"));
    statusLabel->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    timeBoxLayout->addWidget(statusLabel);


    //meter widgets 
    meterGridLayout = new QGridLayout();

    //control buttons
    QHBoxLayout *Buttonlayout = new QHBoxLayout;
    Buttonlayout->setMargin(5);
    Buttonlayout->setSpacing(10);

    recButton = new QPushButton(tr("&Record"), this);
    pauseButton = new QPushButton(tr("&Pause"), this);
    stopButton = new QPushButton(tr("&Stop"), this);

    Buttonlayout->addWidget(recButton);
    Buttonlayout->addWidget(pauseButton);
    Buttonlayout->addWidget(stopButton);

    pauseButton->setEnabled(false);
    stopButton->setEnabled(false);

    QObject::connect(recButton, SIGNAL(clicked()), this, SLOT(recordClicked())); 
    QObject::connect(pauseButton, SIGNAL(clicked()), this, SLOT(pauseClicked())); 
    QObject::connect(stopButton, SIGNAL(clicked()), this, SLOT(stopClicked()));


    //buffer status labels
    QGroupBox *bufferBox = new QGroupBox(tr("Buffer fill rate"));

    QHBoxLayout *bufLabelLayout = new QHBoxLayout;

    QHBoxLayout *currentLayout = new QHBoxLayout;
    QLabel* currentLabel = new QLabel(tr("Current:"), this);
    bufLabel = new QLabel("" /*tr("%1 %").arg(0)*/, this);
    currentLayout->addWidget(currentLabel);
    currentLayout->addWidget(bufLabel);
    currentLayout->addStretch();

    QHBoxLayout *peakLayout = new QHBoxLayout;
    QLabel* peakLabel = new QLabel(tr("Peak:"), this);
    maxBufLabel = new QLabel("" /* tr("%1 % (%2 bytes)").arg(0).arg(0) */, this);
    peakLayout->addWidget(peakLabel);
    peakLayout->addWidget(maxBufLabel);
    peakLayout->addStretch();

    bufLabelLayout->addLayout(currentLayout);
    bufLabelLayout->addLayout(peakLayout);

    bufferBox->setLayout(bufLabelLayout);

    QVBoxLayout *guiBoxLayout = new QVBoxLayout;

    guiBoxLayout->setMargin(4);
    guiBoxLayout->addLayout(fileBoxLayout);
    guiBoxLayout->addLayout(timeBoxLayout);
    guiBoxLayout->addLayout(meterGridLayout);
    guiBoxLayout->addLayout(Buttonlayout);
    //guiBoxLayout->addLayout(bufLabelLayout);
    guiBoxLayout->addWidget(bufferBox);
	QWidget *guiBox = new QWidget;
    guiBox->setLayout(guiBoxLayout);
	
    QMenuBar *menuBar = new QMenuBar; 
    QMenu *filePopup = new QMenu(tr("&File"),this); 
    QMenu *aboutMenu = new QMenu(tr("&Help"),this);
    filePopup->addAction(tr("&New..."), this, SLOT(newFile()));
    filePopup->addAction(tr("&Preferences..."), this, SLOT(settingsClicked()));
    filePopup->addAction(tr("&Quit"), qApp, SLOT(quit()),
            QKeySequence(tr("Ctrl+Q", "File|Quit")));
    aboutMenu->addAction(tr("&About %1...").arg(APP_NAME), this,
            SLOT(helpAbout())); 
    aboutMenu->addAction(tr("&About Qt..."), this,
            SLOT(helpAboutQt())); 
    menuBar->addMenu(filePopup);
    menuBar->addMenu(aboutMenu);

    setWindowTitle(APP_NAME);
    setWindowIcon(QPixmap(qarecord_48_xpm));
    setMenuBar(menuBar);
    setCentralWidget(guiBox);

    timer = new QTimer(this);
    QObject::connect(timer, SIGNAL(timeout()), this, SLOT(timerProc()));
}

void MainWindow::settingsChanged()
{
    // Make sure we're in a sane state first */
    stopClicked();
    captureToggle->setChecked(false);
    freeComponents();

    jackMode = settings->getEnableJack();

    captureToggle->setVisible(!jackMode);

    if (settings->getEnableMidiTrigger()) {
        midiTrigger = new MidiTrigger(*settings);
        QObject::connect(midiTrigger, SIGNAL(record()), this,
            SLOT(recordClicked()));
        QObject::connect(midiTrigger, SIGNAL(pause()), this,
            SLOT(pauseClicked()));
        QObject::connect(midiTrigger, SIGNAL(stop()), this,
            SLOT(stopClicked()));
    }

    ringBuffer = new RingBuffer(settings->getRingBufSize(),
        settings->getChannels(), settings->getEnable32bit());

    updateMeters();

    if (jackMode) {
        jackCapture = new JackCapture(settings);
        jackCapture->setParent(this);
        jackCapture->initJack();
        settings->setRate(jackCapture->getRate());
        jackCapture->activateJack(ringBuffer);
    }
    if (!jackMode) {
        alsaCapture = new Capture(settings, ringBuffer);
        QObject::connect(alsaCapture, SIGNAL(finished()), this,
                SLOT(captureTerminated()));
    }
    diskwrite = new DiskWrite(settings, ringBuffer);

    updateTexts();
    adjustSize();
}

void MainWindow::updateMeters()
{
    QString meterText;
    int l1;

    meters.clear();

    for (l1 = 0; l1 < settings->getChannels(); l1++) {
        QLabel *channelMeterLabel = new QLabel(this);
        channelMeterLabel->setSizePolicy(QSizePolicy::Fixed,
                QSizePolicy::Minimum);

        if (settings->getChannels() == 2)
            meterText = l1 ? tr("R") : tr("L");
        else
            meterText = QString::number(l1+1);

        channelMeterLabel->setText(meterText);
        meterGridLayout->addWidget(channelMeterLabel, l1, 0);
        meters.append(new Meter((tickType) (l1 & 1), ringBuffer, l1,
            settings->getSampleSize(), -settings->getMeterRange(), this));
        meterGridLayout->addWidget(meters[l1], l1, 1);
    }


}

void MainWindow::freeComponents()
{
    while (meterGridLayout->count() > 0) {
        /* These depend on the ring buffer and must be deleted 
           before the ring buffer */
        delete meterGridLayout->itemAt(0)->widget();
    }

    if (midiTrigger != NULL) {
        delete midiTrigger;
        midiTrigger = NULL;
    }

    if (jackCapture != NULL) {
        delete jackCapture;
        jackCapture = NULL;
    }

    if (alsaCapture != NULL) {
        delete alsaCapture;
        alsaCapture = NULL;
    }

    if (diskwrite != NULL) {
        delete diskwrite;
        diskwrite = NULL;
    }

    if (ringBuffer != NULL) {
        delete ringBuffer;
        ringBuffer = NULL;
    }

}


MainWindow::~MainWindow()
{
    freeComponents();
}

void MainWindow::helpAbout()
{
    QMessageBox::about(this, tr("About %1").arg(APP_NAME), ABOUTMSG);
}

void MainWindow::helpAboutQt()
{
    QMessageBox::aboutQt(this, tr("About Qt"));
}

void MainWindow::recordClicked()
{
    if (saveFileName.isEmpty()) {
        newFile();
    }
    if (saveFileName.isEmpty()) {
        return;
    }
    diskwrite->setFileName(saveFileName);
    if (jackMode) {
    } else {
        if (!captureToggle->isChecked()) {
            captureToggle->setChecked(true);
        }
        if (!alsaCapture->isRunning()) {
            alsaCapture->start();
        }
    }
    if (!diskwrite->isRunning()) {
        diskwrite->start();
    }
    diskwrite->setPaused(false);
    if (!jackMode) {
        captureToggle->setEnabled(false);
    }
    timer->start(200);

    statusLabel->setText(tr("Recording"));
    recButton->setEnabled(false);
    pauseButton->setEnabled(true);
    stopButton->setEnabled(true);
}

void MainWindow::stopClicked()
{
    if (diskwrite != NULL) {
        diskwrite->stop();
    }

    captureToggle->setEnabled(true);
    statusLabel->setText(tr("Stopped"));
    recButton->setEnabled(true);
    pauseButton->setEnabled(false);
    stopButton->setEnabled(false);
}

void MainWindow::pauseClicked() 
{
    if (!diskwrite->isRunning()) {
        stopClicked();
        return;
    }
    diskwrite->setPaused(true);
    captureToggle->setEnabled(true);
    statusLabel->setText(tr("Paused"));
    recButton->setEnabled(true);
    pauseButton->setEnabled(false);
    stopButton->setEnabled(true);
}

void MainWindow::newFile()
{
    QString filename = QFileDialog::getSaveFileName(
        this, tr("Choose file name"), lastDir + FILEPRESET, fileFilter);

    if (filename != "") {
        lastDir = filename.left(filename.lastIndexOf('/'));
        if (!filename.endsWith(WAVEEXT))
            filename.append(WAVEEXT);
        saveFileName = filename;
        currentFileLabel->setText(tr("File: %1").arg(saveFileName));
    }
    ringBuffer->reset();
    updateTexts();
}

void MainWindow::timerProc()
{
  if (isRecording()) {
        timer->start(200);
        updateTexts();
    }
}

void MainWindow::updateTexts()
{
    QString qs1, qs2, qs3;
    unsigned long size;
    unsigned long current = 0;
    unsigned long peak = 0;
    QTime seconds;

    if (diskwrite != NULL) {
        seconds = seconds.addSecs(diskwrite->getDataSize() /
            (settings->getFrameSize() * settings->getRate()));
    }
    timeLabel->setText(tr("Time: ") + seconds.toString(tr("H:mm:ss")));

    // update buffer state labels
    if (ringBuffer != NULL) {
        current = ringBuffer->getFillRate();
        peak = ringBuffer->getFillRateMax();
    }
    size = settings->getRingBufSize();

    qs1 = tr("%1 %").arg(100 * current / size);
    bufLabel->setText(qs1);

    if (peak < size) {
        qs1 = tr("%1 % (%2 bytes)").arg(100 * peak / size).arg(peak);
        maxBufLabel->setText(qs1);
    } else {
        maxBufLabel->setText(tr("Buffer overflow"));
    }
}

void MainWindow::captureToggled(bool on)
{
    int l1;

    if (on) {
        if (alsaCapture != NULL) {
            if (!alsaCapture->isRunning()) {
                 alsaCapture->start();
            }
        }
    } else {
        if (alsaCapture != NULL) {
            alsaCapture->stop();
        }
        ringBuffer->reset();
        for (l1 = 0; l1 < meters.size(); l1++) {
            meters[l1]->resetGlobalMax();
        }
    }
}

void MainWindow::captureTerminated()
{
    if (captureToggle->isChecked()) {
        qWarning("ALSA capture failed!");
    }
    pauseClicked();
    captureToggle->setChecked(false);
}

void MainWindow::settingsClicked()
{
    if (!stopRecordingAllowed()) {
        return;
    }

    SettingsDialog *dlg = new SettingsDialog(this);
    dlg->loadFromSettings(*settings);
    if (dlg->exec() == QDialog::Accepted) {
        dlg->saveToSettings(*settings);
        settingsChanged();
    }
    delete dlg;
}

bool MainWindow::stopRecordingAllowed()
{
    if (isRecording()) { 
        if (QMessageBox::question(this, APP_NAME, 
                tr("Stop recording?"),
                QMessageBox::Ok | QMessageBox::Cancel, 
                QMessageBox::Cancel) != QMessageBox::Ok) {
            return false;
        }
    }
    stopClicked();
    return true;
}

bool MainWindow::isRecording()
{
   return diskwrite && diskwrite->isRunning() && 
       (settings->getEnableJack()
       ? jackCapture->isRunning()
       : alsaCapture->isRunning());
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    stopRecordingAllowed() ? event->accept() : event->ignore();
}
