#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include "settings.h"

#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QCheckBox>
#include <QComboBox>
#include <QDialog>
#include <QDialogButtonBox>
#include <QGridLayout>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>
#include <QVBoxLayout>
#include <QWidget>

class SettingsDialogBase : public QObject
{
  Q_OBJECT

public:
    QGridLayout *gridLayout_5;
    QHBoxLayout *topLayout;
    QVBoxLayout *leftOptionsLayout;
    QGroupBox *formatGroupBox;
    QGridLayout *gridLayout_2;
    QLabel *splitLabel;
    QSpinBox *channelsSpin;
    QComboBox *bitComboBox;
    QLabel *channelsLabel;
    QSpinBox *splitSpin;
    QLabel *bitLabel;
    QGroupBox *midiTriggerGroupBox;
    QVBoxLayout *verticalLayout_3;
    QCheckBox *midiTriggerEnabledCheckBox;
    QGridLayout *gridLayout;
    QLabel *midiChannelLabel;
    QSpinBox *midiChannelSpin;
    QLabel *midiNoteLabel;
    QSpinBox *midiNoteSpin;
    QGroupBox *inputGroupBox;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QLabel *driverLabel;
    QComboBox *driverComboBox;
    QGroupBox *alsaGroupBox;
    QGridLayout *gridLayout_3;
    QLabel *alsaDeviceLabel;
    QLabel *rateLabel;
    QSpinBox *rateSpin;
    QLabel *periodSizeLabel;
    QSpinBox *periodSizeSpin;
    QLabel *periodsLabel;
    QSpinBox *periodsSpin;
    QComboBox *alsaDeviceComboBox;
    QGridLayout *gridLayout_4;
    QLabel *ringBufferLabel;
    QSpinBox *ringBufferSpin;
    QLabel *meterRangeLabel;
    QSpinBox *meterRangeSpin;
    QWidget *rightSpacerWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *resetButton;
    QPushButton *loadButton;
    QPushButton *saveButton;
    QWidget *spacerWidget;
    QDialogButtonBox *buttonBox;

    public:
      void setupUi(QDialog *SettingsDialogBase);
      void retranslateUi(QDialog *SettingsDialogBase);
};


class SettingsDialog : public QDialog
{
  Q_OBJECT

  public:
    SettingsDialog(QWidget *parent = 0);

    void loadFromSettings(SettingsData& settings);
    void saveToSettings(SettingsData& settings);

  private:
    SettingsDialogBase ui;

  public slots:
    void updateEnabled();
    void resetToDefault();
    void loadFromFile();
    void saveToFile();
};

#endif
