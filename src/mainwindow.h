#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QLabel>
#include <QMessageBox>
#include <QCheckBox>
#include <QList>
#include <QVector>
#include <QTimer>
#include <QMainWindow>
#include <QApplication>
#include <QGridLayout>
#include <QCloseEvent>

#include "capture.h"
#include "jackcapture.h"
#include "ringbuffer.h"
#include "diskwrite.h"
#include "meter.h"
#include "miditrigger.h"
#include "settings.h"

#ifdef HAVE_CONFIG_H
#include "config.h"
static const char ABOUTMSG[] = APP_NAME " " PACKAGE_VERSION "\n"
#else
static const char ABOUTMSG[] = "QARecord\n"
#endif
                          "(C) 2002-2003 Matthias Nagorni (SuSE AG Nuremberg)\n"
			  "(C) 2009 Frank Kober\n"
			  "(C) 2009 David Henningsson\n"
			  "(C) 2009 Guido Scholz\n\n"
                          APP_NAME " is licensed under the GPL.\n";


class MainWindow : public QMainWindow
{
  Q_OBJECT

  private:
    QLabel *currentFileLabel, *timeLabel, *bufLabel, *maxBufLabel, *statusLabel;
    QPushButton *recButton, *pauseButton, *stopButton;
    QCheckBox *captureToggle;
    QTimer *timer;
    QGridLayout *meterGridLayout;
    Capture *alsaCapture;
    JackCapture *jackCapture;
    RingBuffer *ringBuffer;
    DiskWrite *diskwrite;
    SettingsData *settings;
    QVector<Meter*> meters;
    MidiTrigger *midiTrigger;
    bool jackMode;
    QString saveFileName;
    QString lastDir;
    QString fileFilter;

    void createBaseGUI();
    void settingsChanged();
    void updateMeters();
    void freeComponents();
    void updateTexts();
    bool isRecording();
    bool stopRecordingAllowed();

  protected:
    void closeEvent(QCloseEvent *event);

  public:
    MainWindow(SettingsData* p_settings);
    ~MainWindow();

  public slots:
    void captureTerminated();
    void helpAbout();
    void helpAboutQt();
    void newFile();
    void recordClicked();
    void stopClicked();
    void pauseClicked();
    void timerProc();
    void settingsClicked();
    void captureToggled(bool on);
};
  
#endif
