#include <QApplication>
#include <QTranslator>
#include <QLocale>
#include <QLibraryInfo>

#include "mainwindow.h"
#include "settings.h"
#include "config.h"


int main(int argc, char *argv[])  
{
    QApplication app(argc, argv);

    // translator for Qt library messages
    QTranslator qtTr;
    QLocale loc = QLocale::system();

    if (qtTr.load(QString("qt_") + loc.name(),
                QLibraryInfo::location(QLibraryInfo::TranslationsPath)))
        app.installTranslator(&qtTr);

    // translator for qarecord messages       
    QTranslator qarecordTr;

    if (qarecordTr.load(QString(PACKAGE "_") + loc.name(), TRANSLATIONSDIR))
        app.installTranslator(&qarecordTr);

    // Parse settings
    SettingsData settings;
    settings.load();
    if (!settings.parseCmdLine(argc, argv)) {
        exit(EXIT_SUCCESS);
    }
    settings.validateSettings();

    // Run GUI
    MainWindow* top = new MainWindow(&settings);
    int retval = app.exec();
    delete top;
    return retval;
}
