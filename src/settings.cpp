#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <getopt.h>

#include "settings.h"
#include "mainwindow.h"

// Option names - do not translate
static const char ENABLE32BIT_O[] = "32bit";
static const char RINGBUFSIZE_O[] = "buffersize";
static const char CHANNELS_O[] = "channels";
static const char ENABLEJACK_O[] = "jack";
static const char METERRANGE_O[] = "meterrange";
static const char SPLITMB_O[] = "split";

static const char ALSAPCMNAME_O[] = "device";
static const char ALSAPERIODS_O[] = "fragments";
static const char ALSAPERIODSIZE_O[] = "periodsize";
static const char RATE_O[] = "rate";

static const char DISABLEMIDITRIGGER_O[] = "nomiditrigger";
static const char MIDICHANNEL_O[] = "midichannel";
static const char MIDINOTE_O[] = "midinote";

static const char KS[] = "=";
static const char QARECORDRC[] = ".qarecordrc";

static struct option options[] =
// Standard options
        {{"help", 0, 0, 'h'},
         {"version", 0, 0, 'v'},
// General options
         {ENABLE32BIT_O, 0, 0, 'i'},
         {RINGBUFSIZE_O, 1, 0, 'b'},
         {CHANNELS_O, 1, 0, 'c'},
         {ENABLEJACK_O, 0, 0, 'j'},
         {METERRANGE_O, 1, 0, 'e'},
         {SPLITMB_O, 1, 0, 's'},
// Alsa options
         {ALSAPCMNAME_O, 1, 0, 'd'},
         {ALSAPERIODS_O, 1, 0, 'f'},
         {ALSAPERIODSIZE_O, 1, 0, 'p'},
         {RATE_O, 1, 0, 'r'},
// Midi trigger options
         {MIDICHANNEL_O, 1, 0, 'm'},
         {MIDINOTE_O, 1, 0, 'n'},
         {DISABLEMIDITRIGGER_O, 0, 0, 't'},
         {0, 0, 0, 0}};

SettingsData::SettingsData()
{
    setDefault();
}

static int checkRange(const char* n, int val, int minval, int maxval)
{
    if (val < minval) {
         qWarning("Changed %s to %d (valid range is %d to %d)", n,
                 minval, minval, maxval);
         return minval;
    }
    if (val > maxval) {
         qWarning("Changed %s to %d (valid range is %d to %d)", n,
                 maxval, minval, maxval);
         return maxval;
    }
    return val;
}

void SettingsData::validateSettings()
{
    channels = checkRange(CHANNELS_O, channels, 1, SHRT_MAX);

    ringBufSize = checkRange(RINGBUFSIZE_O, ringBufSize, 4096 * getFrameSize(),
            INT_MAX / 2);

    if (ringBufSize % getFrameSize() != 0) {
        ringBufSize += getFrameSize() - ringBufSize % getFrameSize();
        qWarning("Changed buffersize to %d (must be multiple of frame size)",
                (int) ringBufSize);
    }

    midiChannel = checkRange(MIDICHANNEL_O, midiChannel, 1, 16);
    midiNote = checkRange(MIDINOTE_O, midiNote, 0, 127);
    meterRange = checkRange(METERRANGE_O, meterRange, 12, 192);
    splitMB = checkRange(SPLITMB_O, splitMB, 1, 4095);

    /* Alsa parameters are checked in the alsa capture driver */
}


void SettingsData::setDefault()
{
    ringBufSize = 1048576;
    enableJack = false;
    enable32bit = false;
    disableMidiTrigger = false;
    midiChannel = 16;
    midiNote = 21;
    channels = 2;
    meterRange = 85;
    splitMB = 2000;

    rate = 44100;
    alsaPeriods = 2;
    alsaPeriodsize = 2048;
    alsaPcmName = "plughw:0";
}

void SettingsData::printHelp(QTextStream& out)
{
    SettingsData def;

    out << ABOUTMSG << endl;
    out << "General options:" << endl;
    out << "--32bit                  Use 32 Bit format" << endl;
    out << QString("--buffersize <bytes>     Size of ringbuffer [%1]")
        .arg(def.ringBufSize) << endl;
    out << QString("--channels <num>         Channels [%1]")
        .arg(def.channels) << endl;
    out << "--jack                   Enable JACK mode" << endl;
    out << QString("--meterrange <dB>        Dynamic range of peak meter [%1]")
        .arg(def.meterRange) << endl;
    out << QString("--split <MB>             Maximum size of wave file [%1]")
        .arg(def.splitMB) << endl << endl;

    out << "ALSA specific options:" << endl;
    out << QString("--device <ALSA device>   ALSA Capture device [%1]")
        .arg(def.alsaPcmName) << endl;
    out << QString("--fragments <num>        Number of fragments [%1]")
        .arg(def.alsaPeriods) << endl;
    out << QString("--periodsize <frames>    Periodsize [%1]")
        .arg(def.alsaPeriodsize) << endl;
    out << QString("--rate <num>             Sample rate [%1]")
        .arg(def.rate) << endl << endl;

    out << "MIDI Trigger options:" << endl;
    out << "--noMidiTrigger          Disable MIDI trigger" << endl;
    out << QString("--midiChannel <num>      MIDI Channel [%1]")
        .arg(def.midiChannel) << endl;
    out << QString("--midiNote <num>         MIDI Note [%1]")
        .arg(def.midiNote) << endl << endl;


    out << "Standard options:" << endl;
    out << "--help                   Print possible command-line options "
        "and exit" << endl;
    out << "--version                Print version information and exit"
        << endl;
}

int SettingsData::parseCmdLine(int argc, char *argv[])
{
    int getopt_return;
    int option_index;

    QTextStream out(stdout);

    while((getopt_return = getopt_long(argc, argv, "jhvtid:b:f:p:m:n:r:c:e:s:",
        options, &option_index)) >= 0) 
    {
        switch(getopt_return) {
            case 'd':
                alsaPcmName = QString(optarg);
                break;
            case 'b':
                ringBufSize = atoi(optarg);
                break;
            case 'c':
                channels = atoi(optarg);
                break;
            case 'e':
                meterRange = atoi(optarg);
                break;
            case 'f':
                alsaPeriods = atoi(optarg);
                break;
            case 'p':
                alsaPeriodsize = atoi(optarg);
                break;
            case 't':
                disableMidiTrigger = true;
                break;
            case 'm':
                midiChannel = atoi(optarg);
                break;
            case 's':
                splitMB = atoi(optarg);
                break;
            case 'r':
                rate = atoi(optarg);
                break;
            case 'j': 
                enableJack = true;
                break;
            case 'i': 
                enable32bit = true;
                break;
            case 'n':
                midiNote = atoi(optarg);
                break;
            case 'v':
                out << ABOUTMSG;
                return 0;
            case 'h':
                printHelp(out);
                return 0;
        }
    }
    return 1;
}


void SettingsData::load()
{
    QFile file(QDir::homePath() + "/" + QARECORDRC);

    /* if no configuration file is found, just keep defaults */
    if (!file.open(QIODevice::ReadOnly))
        return;

    QTextStream ts(&file);
    QString s, key, value;

    while (!ts.atEnd()) {
        s = ts.readLine();
        if (!s.startsWith("#")) {
            key = s.section(KS, 0, 0);
            value = s.section(KS, 1).trimmed();

            if (key.compare(ENABLE32BIT_O) == 0) {
                enable32bit = value.toInt();
            }
            else if (key.compare(RINGBUFSIZE_O) == 0) {
                ringBufSize = value.toInt();
            }
            else if (key.compare(CHANNELS_O) == 0) {
                channels = value.toInt();
            }
            else if (key.compare(ENABLEJACK_O) == 0) {
                enableJack = value.toInt();
            }
            else if (key.compare(METERRANGE_O) == 0) {
                meterRange = value.toInt();
            }
            else if (key.compare(SPLITMB_O) == 0) {
                splitMB = value.toInt();
            }
            else if (key.compare(ALSAPCMNAME_O) == 0) {
                alsaPcmName = value;
            }
            else if (key.compare(ALSAPERIODS_O) == 0) {
                alsaPeriods = value.toInt();
            }
            else if (key.compare(ALSAPERIODSIZE_O) == 0) {
                alsaPeriodsize = value.toInt();
            }
            else if (key.compare(RATE_O) == 0) {
                rate = value.toInt();
            }
            else if (key.compare(DISABLEMIDITRIGGER_O) == 0) {
                disableMidiTrigger = value.toInt();
            }
            else if (key.compare(MIDICHANNEL_O) == 0) {
                midiChannel = value.toInt();
            }
            else if (key.compare(MIDINOTE_O) == 0) {
                midiNote = value.toInt();
            }
        }
    }
    file.close();
}

void SettingsData::save()
{
    QFile file(QDir::homePath() + '/' + QARECORDRC);
    
    if (!file.open(QIODevice::WriteOnly)) {
        qWarning("Error: Could not save configuration"
                    " file: ~/%s", QARECORDRC);
        return;
    }

    QDateTime dt = QDateTime::currentDateTime();
    QTextStream ts(&file);

    ts << "# " PACKAGE " configuration file" << endl
       << "# last modified: " << dt.toString(Qt::ISODate) << endl
       << "#" << endl
       << ENABLE32BIT_O << KS << enable32bit << endl
       << RINGBUFSIZE_O << KS << ringBufSize << endl
       << CHANNELS_O << KS << channels << endl
       << ENABLEJACK_O << KS << enableJack << endl
       << METERRANGE_O << KS << meterRange << endl
       << SPLITMB_O << KS << splitMB << endl

       << ALSAPCMNAME_O << KS << alsaPcmName << endl
       << ALSAPERIODS_O << KS << alsaPeriods << endl
       << ALSAPERIODSIZE_O << KS << alsaPeriodsize << endl
       << RATE_O << KS << rate << endl

       << DISABLEMIDITRIGGER_O << KS << disableMidiTrigger << endl
       << MIDICHANNEL_O << KS << midiChannel << endl
       << MIDINOTE_O << KS << midiNote << endl;

    file.close();
}
