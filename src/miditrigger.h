#include <QObject>
#include <QSocketNotifier>
#include <alsa/asoundlib.h>
#include "settings.h"


class MidiTrigger : public QObject
{
  Q_OBJECT

  private:
    int midiChannel, recordNote, pauseNote, stopNote;

    snd_seq_t *seq_handle;
    bool seq_handle_allocated;
    int in_port; 
    QSocketNotifier *seqNotifier;

    int initSeqNotifier();
    int open_seq();

  public:
    MidiTrigger(SettingsData& settings);
    ~MidiTrigger();

  public slots:
    void midiAction();

  signals:
    void record();
    void pause();
    void stop();

};
